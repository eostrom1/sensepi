#!/usr/bin/env python3
import subprocess
from sense_hat import SenseHat
from time import sleep

def get_t_sense_c(sense):
    return (sense.get_temperature_from_humidity() + sense.get_temperature_from_pressure())/2

def get_t_cpu_c():
    return float(subprocess.check_output("vcgencmd measure_temp", shell=True).decode('utf8').split('=')[1].split("'")[0])

def get_t_real_c(t_sense_c, t_cpu_c, t_factor):
    return t_sense - ((t_cpu_c - t_sense_c)/t_factor)

def get_t_real_f(t_sense_c, t_cpu_c, t_factor):
    return (t_sense_c - ((t_cpu_c - t_sense_c)/t_factor))*(9/5)+32

def sense_show_humidity(sense):
    sense.show_letter("H")
    sleep(1)
    sense.show_message(str(round(sense.get_humidity(),1))+"%")

def sense_show_temperature(sense):
    sense.show_letter("T")
    sleep(1)
    sense.show_message(str(round(get_t_real_f(get_t_sense_c(sense), get_t_cpu_c(), 2.41),1))+"f")

def main():
    sense = SenseHat()
    sense.set_rotation(180)
    while True:
        sense_show_humidity(sense)
        sense_show_temperature(sense)

if __name__ == "__main__":
    main()
